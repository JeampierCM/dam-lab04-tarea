import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
//Icons
import { MaterialCommunityIcons } from "@expo/vector-icons";

//screens
import EventosScreens from "./screens/EventosScreens"
import InfoEventosScreens from "./screens/InfoEventosScreens"

//screens Stack Login

const LoginStackNavigator = createNativeStackNavigator();
const LoginStack = () => {
	return (
		<LoginStackNavigator.Navigator initialRouteName="Eventos">
			<LoginStackNavigator.Screen name="Eventos" component={EventosScreens} />
			<LoginStackNavigator.Screen name="EventoInfo" component={InfoEventosScreens} />
		</LoginStackNavigator.Navigator>
	);
}

//Tab
const Tab = createBottomTabNavigator();

const MyTabs = () => {
	return (
		<Tab.Navigator
			initialRouteName="Login"
			screenOptions={{ tabBarActiveTintColor: "purple" }}>
			<Tab.Screen
				name="Eventos"
				component={EventosScreens}
				options={{
					tabBarIcon: ({ color, size }) => (
						<MaterialCommunityIcons name="event" size={size} color={color} />
					),
				}}
			/>
      <Tab.Screen
				name="EventoInfo"
				component={InfoEventosScreens}
				options={{
					tabBarIcon: ({ color, size }) => (
						<MaterialCommunityIcons name="event" size={size} color={color} />
					),
				}}
			/>
		</Tab.Navigator>
	);
}

const Navegation = () => {
	return (
		<NavigationContainer>
			<MyTabs />
		</NavigationContainer>
	);
}

export default Navegation;